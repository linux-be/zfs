/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2017-2018 Witaut Bajaryn
 */


/*
 * System includes
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/systeminfo.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <libintl.h>

#include <libbe.h>
#include <libbe_priv.h>

/*
 * Function:	be_regenerate_menu
 * Description:	Runs grub-mkconfig to regenerate GRUB2's boot menu.
 * Parameter:
 *		None
 * Returns:
 *		BE_SUCCESS - Success
 *		BE_ERR_REGENERATE_MENU - Failure
 * Scope:
 *		Semi-private (library wide use only)
 */
int
be_regenerate_menu(void)
{
	struct be_defaults be_defaults;
	char *regenerate_menu_cmd;

	char stderr_buf[BUFSIZ];
	char stdout_buf[BUFSIZ];
	int ret;

	be_get_defaults(&be_defaults);
	regenerate_menu_cmd = be_defaults.be_deflt_regenerate_menu_command;

	ret = be_run_cmd(regenerate_menu_cmd,
	    stderr_buf, sizeof (stderr_buf), stdout_buf, sizeof (stdout_buf));

	if (ret != BE_SUCCESS) {
		be_print_err(gettext("be_regenerate_menu: "
		    "failed to regenerate the boot menu.\n"));
		ret = BE_ERR_REGENERATE_MENU;
	}

	be_print_cmd_output(regenerate_menu_cmd, stderr_buf, stdout_buf);

	return (ret);
}

/*
 * Function:	be_get_pool_guid
 * Description:	Gets the GUID of the pool the BE is on.
 * Parameters:
 *		be_root_ds - The main dataset for the BE.
 *		guid - A pointer to an int64_t to be set to the GUID
 *			upon success.
 * Returns:
 *		BE_SUCCESS - Success
 *		be_errno_t - Failure
 * Scope:
 *		Private
 */
int
be_get_pool_guid(const char *be_root_ds, uint64_t *guid)
{
	zfs_handle_t *zhp = NULL;

	if ((zhp = zfs_open(g_zfs, be_root_ds, ZFS_TYPE_FILESYSTEM)) == NULL) {
		be_print_err(gettext("be_get_pool_guid: failed to open "
		    "dataset (%s): %s\n"), be_root_ds,
		    libzfs_error_description(g_zfs));
		return (zfs_err_to_be_err(g_zfs));
	}

	*guid = zpool_get_prop_int(zfs_get_pool_handle(zhp), ZPOOL_PROP_GUID,
	    NULL);
	ZFS_CLOSE(zhp);
	return (BE_SUCCESS);
}

/*
 * Function:	be_escape_be_name
 * Description:	Escapes a BE name to match GRUB2 unique menu ID format.
 *		The format does not allow '.' and ':' characters.
 *		They are replaced by "_0" and "_1", respectively,
 *		and '_' is replaced by "__".
 * Parameters:
 *		be_name - The name of the BE.
 *		escaped_be_name - A pointer to a char pointer to be set
 *			to the escaped BE name upon success
 *			or to NULL upon failure.
 *			The caller is responsible for freeing it.
 * Returns:
 *		BE_SUCCESS - Success
 *		BE_ERR_NOMEM - escaped_be_name allocation failure
 * Scope:
 *		Private
 */
int
be_escape_be_name(const char *be_name, char **escaped_be_name)
{
	int be_name_len = strlen(be_name);
	const char *src;
	char *dest;

	*escaped_be_name = malloc(1 + 2 * be_name_len *
	    sizeof (**escaped_be_name));
	if (*escaped_be_name == NULL) {
		be_print_err(gettext("be_escape_be_name: malloc failed\n"));
		return (BE_ERR_NOMEM);
	}

	for (src = be_name, dest = *escaped_be_name; *src != '\0'; src++) {
		if (*src == '_') {
			*dest++ = '_';
			*dest++ = '_';
		} else if (*src == ':') {
			*dest++ = '_';
			*dest++ = '1';
		} else if (*src == '.') {
			*dest++ = '_';
			*dest++ = '0';
		} else {
			*dest++ = *src;
		}
	}
	*dest = '\0';

	return (BE_SUCCESS);
}

/*
 * Function:	be_make_menu_entry_id
 * Description:	Makes a unique ID for the BE's boot menu entry.
 *		The ID should match the one used by the grub.d script
 *		that generates entries for BEs.
 *		The format is '<prefix>-<pool GUID in hex>-<escaped BE name>'.
 *		"<escaped BE name>" is produced by be_escape_be_name().
 * Parameters:
 *		menu_entry_id - A pointer to a char pointer to be set
 *			to the menu entry ID upon success
 *			or to NULL upon failure.
 *			The caller is responsible for freeing it.
 *		be_root_ds - The main dataset for the BE.
 *		be_name - The name of the BE.
 * Returns:
 *		BE_SUCCESS - Success
 *		be_errno_t - Failure
 * Scope:
 *		Private
 */
int
be_make_menu_entry_id(
	char **menu_entry_id,
	const char *be_root_ds,
	const char *be_name)
{
	uint64_t pool_guid = 0;
	char *escaped_be_name = NULL;
	int ret = BE_SUCCESS;

	if ((ret = be_get_pool_guid(be_root_ds, &pool_guid)) != BE_SUCCESS) {
		be_print_err(gettext("be_make_menu_entry_id: "
		    "failed to get the pool GUID: %s\n"), be_err_to_str(ret));
		return (ret);
	}

	if ((ret = be_escape_be_name(be_name, &escaped_be_name))
	    != BE_SUCCESS) {
		be_print_err(gettext("be_make_menu_entry_id: "
		    "failed to escape the BE name: %s\n"), be_err_to_str(ret));
		return (ret);
	}

	if (asprintf(menu_entry_id, "%s-%llx-%s", BE_GRUB2_MENU_ENTRY_ID_PREFIX,
	    pool_guid, escaped_be_name) < 0) {
		int err = errno;
		be_print_err(gettext("be_make_menu_entry_id: "
		    "failed to format the menu entry name: %s\n"),
		    strerror(err));
		*menu_entry_id = NULL;
		ret = errno_to_be_err(err);
	}

	free(escaped_be_name);
	return (ret);
}

/*
 * Function:	be_change_grub2_default
 * Description:	Runs grub-set-default to make the BE's menu entry the default.
 * Parameters:
 *		be_root_ds - The main dataset for the BE.
 *		be_name - The name of the BE.
 * Returns:
 *		BE_SUCCESS - Success
 *		BE_ERR_CHANGE_GRUB2_DEFAULT - Failure
 * Scope:
 *		Semi-private (library wide use only)
 */
int
be_change_grub2_default(char *be_root_ds, char *be_name)
{
	struct be_defaults be_defaults;
	char *set_default_cmd_name;

	char *menu_entry_id = NULL;

	char *set_default_cmd_format = "%s \"%s\"";
	char *set_default_cmd = NULL;

	char stderr_buf[BUFSIZ];
	char stdout_buf[BUFSIZ];

	int err;
	int ret = BE_SUCCESS;

	be_get_defaults(&be_defaults);
	set_default_cmd_name = be_defaults.be_deflt_set_default_command;

	err = be_make_menu_entry_id(&menu_entry_id, be_root_ds, be_name);
	if (err != BE_SUCCESS) {
		be_print_err(gettext("be_change_grub2_default: "
		    "failed to make the menu entry name: %s\n"),
		    be_err_to_str(err));
		ret = BE_ERR_CHANGE_GRUB2_DEFAULT;
		goto out;
	}

	if (asprintf(&set_default_cmd, set_default_cmd_format,
	    set_default_cmd_name, menu_entry_id) < 0) {
		err = errno;
		be_print_err(gettext("be_change_grub2_default: "
		    "failed to format the command: %s\n"), strerror(err));

		/*
		 * The value is undefined, so set it to NULL
		 * so that free() can be called on it.
		 */
		set_default_cmd = NULL;
		ret = BE_ERR_CHANGE_GRUB2_DEFAULT;
		goto out;
	}

	err = be_run_cmd(set_default_cmd,
	    stderr_buf, sizeof (stderr_buf), stdout_buf, sizeof (stdout_buf));
	if (err != BE_SUCCESS) {
		be_print_err(gettext("be_change_grub2_default: "
		    "failed to change the default boot menu entry: %s\n"),
		    be_err_to_str(err));
		ret = BE_ERR_CHANGE_GRUB2_DEFAULT;
	}

	be_print_cmd_output(set_default_cmd, stderr_buf, stdout_buf);

out:
	free(set_default_cmd);
	free(menu_entry_id);
	return (ret);
}
