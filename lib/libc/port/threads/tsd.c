/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#include "lint.h"
#include <stddef.h>
#include <atomic.h>
#include <pthread_illumos.h>

/*
 * 128 million keys should be enough for anyone.
 * This allocates half a gigabyte of memory for the keys themselves and
 * half a gigabyte of memory for each thread that uses the largest key.
 */
#define	MAX_KEYS	0x08000000U

/*
 * Same as pthread_key_create(), except that the key creation
 * is performed only once.  This relies upon the fact that a key
 * value of THR_ONCE_KEY is invalid, and requires that the key be
 * allocated with a value of THR_ONCE_KEY before calling here.
 * THR_ONCE_KEY and PTHREAD_ONCE_KEY_NP, defined in <thread.h>
 * and <pthread.h> respectively, must have the same value.
 * Example:
 *
 *	static pthread_key_t key = PTHREAD_ONCE_KEY_NP;
 *	...
 *	pthread_key_create_once_np(&key, destructor);
 */
#pragma weak pthread_key_create_once_np = thr_keycreate_once
int
thr_keycreate_once(pthread_key_t *keyp, void (*destructor)(void *))
{
	static pthread_mutex_t key_lock = PTHREAD_MUTEX_INITIALIZER;
	pthread_key_t key;
	int error;

	if (*keyp == PTHREAD_ONCE_KEY_NP) {
		pthread_mutex_lock(&key_lock);
		if (*keyp == PTHREAD_ONCE_KEY_NP) {
			error = pthread_key_create(&key, destructor);
			if (error) {
				pthread_mutex_unlock(&key_lock);
				return (error);
			}
			membar_producer();
			*keyp = key;
		}
		pthread_mutex_unlock(&key_lock);
	}
	membar_consumer();

	return (0);
}
