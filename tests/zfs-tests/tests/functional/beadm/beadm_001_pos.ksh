#!/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright (c) 2017 by Witaut Bajaryn
#

. $STF_SUITE/tests/functional/beadm/beadm_common.shlib

#
# DESCRIPTION:
#	Verify that beadm list shows a manually-created boot environment.
#
# STRATEGY:
#	1. Create a BE and some child datasets with 'zfs'.
#	2. Run 'beadm list' and verify it succeeds.
#	3. Run it again and verify the output matches a glob pattern.
#

verify_runnable "global"

log_assert "beadm list shows a manually-created boot environment."

log_onexit destroy_be_container
create_be_manually $TESTBE

log_must beadm list

typeset output=$(beadm list)
if [[ "$output" != \
BE+( )Active+( )Mountpoint+( )Space+( )Policy+( )Created$'\n'\
$TESTBE+( )-+( )-+( )+([0-9.])[KM]+( )static+( )\
{4}([0-9])-{2}([0-9])-{2}([0-9])\ {2}([0-9]):{2}([0-9]) ]]; then
	log_fail "beadm list output doesn't match the pattern."
fi

log_pass "beadm list shows a manually-created boot environment as expected."
