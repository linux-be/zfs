#!/usr/bin/expect -f

proc exp_timeout { id msg } {
    expect {
        -i $id
        $msg {}
        timeout {
            error "Timeout when expecting: $msg"
        }
    }
}

set timeout 120

proc exp_prompt { id } {
    exp_timeout $id "~ #"
}

proc exp_login { id } {
    exp_timeout $id {Linux version}
    exp_timeout $id {Importing ZFS pool ci}
    exp_timeout $id {login:}

    send -i $id "root\r"
    exp_timeout $id {Password:}
    send -i $id "araara\r"

    exp_prompt $id
}

proc exp_cmd { id msg } {
    send -i $id "$msg"
    send -i $id "\r"
    exp_prompt $id
    exp_last_cmd_success $id
}

proc exp_last_cmd_success { id } {
    send -i $id "echo $?\r"

    expect {
        -i $id
        -re {(?n)^(\d+)\r\n} {
	    set exit_code "$expect_out(1,string)"
	}
        timeout {
            error "Timeout when expecting an exit code."
        }
    }
    exp_prompt $id

    if {$exit_code ne "0"} {
        error "The command has failed with exit code $exit_code."
    }
}

proc exp_export { id key } {
    exp_cmd $id "export $key=$::env($key)"
}
