/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2014 Garrett D'Amore <garrett@damore.org>
 * Copyright 2016 Joyent, Inc.
 *
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PTHREAD_ILLUMOS_H
#define	_PTHREAD_ILLUMOS_H

#include <sys/feature_tests.h>

#ifndef	_ASM
#include <sys/types.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#endif	/* _ASM */

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * The key to be created by pthread_key_create_once_np()
 * must be statically initialized with PTHREAD_ONCE_KEY_NP.
 * This must be the same as THR_ONCE_KEY in <thread.h>
 */
#define	PTHREAD_ONCE_KEY_NP	(pthread_key_t)(-1)

#ifndef	_ASM

/*
 * function prototypes - thread related calls
 */

extern int pthread_key_create_once_np(pthread_key_t *, void (*)(void *));

#endif	/* _ASM */

#ifdef	__cplusplus
}
#endif

#endif	/* _PTHREAD_ILLUMOS_H */
